#!/usr/bin/env bash

### Determine current NVIDIA driver version.
### Latest version (or pointer to same) @ https://bitbucket.org/tlroche/linux_version_getters

### Copyright (C) 2017 Tom Roche <Tom_Roche@pobox.com>
### This work is licensed under the Creative Commons Attribution 4.0 International License.
### To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/

### ----------------------------------------------------------------------
### constants
### ----------------------------------------------------------------------

## messaging

THIS_FP="${0}"
THIS_DIR="$(readlink -f $(dirname ${THIS_FP}))" # FQ/absolute path
THIS_FN="$(basename ${THIS_FP})"
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

## utility

NV_QUERY_TOOL='nvidia-settings'
NV_QUERY_CL=''
NV_VER=''

### ----------------------------------------------------------------------
### payload
### ----------------------------------------------------------------------

if [[ -z "$(which ${NV_QUERY_TOOL})" ]] ; then
    >&2 echo "${ERROR_PREFIX} NVIDIA driver version query tool='${NV_QUERY_TOOL}' not installed, exiting ..."
    exit 2
else

    NV_VER_LEN="$(${NV_QUERY_TOOL} --version | wc -l)"
    if [[ -z "${NV_VER_LEN}" ]] ; then
        >&2 echo "${ERROR_PREFIX} NVIDIA driver version simple query failed, exiting ..."
        exit 3
    else
        ### TODO: test numeric!
        NV_TAIL_LEN=$(( NV_VER_LEN - 1 ))
        NV_QUERY_CL="${NV_QUERY_TOOL} --version | tail -n ${NV_TAIL_LEN} | head -n 1"
        NV_VER="$(eval ${NV_QUERY_CL})"
        if [[ -z "${NV_VER}" ]] ; then
            >&2 echo "${ERROR_PREFIX} NVIDIA driver version string query failed, exiting ..."
            exit 4
        else
            echo "${NV_VER}"
            exit 0
        fi
    fi

fi
exit 1
