#!/usr/bin/env bash

### Determine current window manager (WM) and version.
### Usage as described in section=testcases below.

### Following attempts to observe Google Shell Style Guide currently @ https://google.github.io/styleguide/shell.xml

### Copyright (C) 2017 Tom Roche <Tom_Roche@pobox.com>
### This work is licensed under the Creative Commons Attribution 4.0 International License.
### To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
### Latest version of this file (or pointer to same) should be available @
### https://bitbucket.org/tlroche/linux_version_getters/src/HEAD/get_wm_version.sh?at=master

### Currently very incomplete: pull requests welcome!
### TODO:

### 1. add executable testcases (see repos/.../templates/bash_tests_template.sh)

### 2. WM commandlines:

### 2.1. Xfce: handle older (and presumably thus newer) major versions

### 2.2. allow extensibility, e.g., via .properties file

### 2.3. get more, i.e., for "all major window managers"

### ----------------------------------------------------------------------
### constants
### ----------------------------------------------------------------------

### Messaging constants

declare -r THIS_FP="${BASH_SOURCE}"
declare -r THIS_FN="$(basename ${THIS_FP})"
declare -r THIS_DIR="$(readlink -f $(dirname ${THIS_FP}))"  # FQ path to caller's $(pwd)
declare -r MESSAGE_PREFIX="${THIS_FN}:"
declare -r ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
declare -r VERSION_N='0.1'
declare -r VERSION_STR="${THIS_FN} version=${VERSION_N}"

### WM commandlines

declare -r X_DISPLAY=':0'
declare -r X_DISPLAY_OPT="--display='${X_DISPLAY}'"

## Cinnamon: simple! Works in both tty1 and tty7! Me like!

declare -r CINNAMON_VER_CL='cinnamon --version'

## MATE: somewhat more complicated

# declare -r MATE_VER_CL='mate-about --version' # works only in {GUI, tty8} (not tty7 ???)
declare -r MATE_VER_CL="mate-about ${X_DISPLAY_OPT} --version"

## Xfce: even more complicated: see function get_Xfce_version

declare -r CURRENT_XFCE_MAJOR_VER='4'
declare -r CURRENT_XFCE_WM_NAME="xfwm${CURRENT_XFCE_MAJOR_VER}"
declare -r CURRENT_XFCE_EXEC_NAME="${CURRENT_XFCE_WM_NAME}"
# declare -r CURRENT_XFCE_EXEC_NAME="${CURRENT_XFCE_WM_NAME}-settings" # also works, though differently
# declare -r XFCE_VER_CL="${CURRENT_XFCE_EXEC_NAME} --version"         # fails from tty1
declare -r XFCE_VER_CL="${CURRENT_XFCE_EXEC_NAME} ${X_DISPLAY_OPT} --version"

### output

wm_ver='' # ~= retval for this script

### -----------------------------------------------------------------------
### functions
### -----------------------------------------------------------------------

### Get version for Cinnamon: should work for all versions date < 2018 and possibly after.
function get_Cinnamon_version() {

    ## note local copies of readonly globals are forbidden in some bash versions :-(
    local -r FN_MESSAGE_PREFIX="${MESSAGE_PREFIX}::${FUNCNAME[0]}:"
    local -r FN_ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
    local ret_raw=''
#    local ret_val='' # no need: Cinnamon devs return exactly what we want

    ret_raw="$(eval "${CINNAMON_VER_CL}" 2> /dev/null)"
    if [[ -z "${ret_raw}" ]] ; then
        ## signal error, but recoverably: let caller do the messaging
        echo ''
        return # value of 'last command executed within the function or script'
    fi

    ## if [[ -n "${ret_raw}" ]] ; then
    ## return it. TODO: test for tri-numericity ~= N.N.N
    echo "${ret_raw}"

} # end function get_Cinnamon_version

### Get version for MATE: should work for all versions date < 2018 and possibly after.
function get_MATE_version() {

    ## note local copies of readonly globals are forbidden in some bash versions :-(
    local -r FN_MESSAGE_PREFIX="${MESSAGE_PREFIX}::${FUNCNAME[0]}:"
    local -r FN_ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
    local ret_raw='' # MATE devs return slightly more than what we want
    local ver_num=''
#    local ret_val=''

    ret_raw="$(eval "${MATE_VER_CL}" 2> /dev/null)"
    if [[ -z "${ret_raw}" ]] ; then
        ## signal error, but recoverably: let caller do the messaging
        echo ''
        return # value of 'last command executed within the function or script'
    fi

    ## if [[ -n "${ret_raw}" ]] ; then
    ## strip bit of "header"
    ver_num="${ret_raw#MATE Desktop Environment }"
#    echo "${MESSAGE_PREFIX} DEBUG: ver_num='${ver_num}'"
    if [[ -z "${ver_num}" ]] ; then
        ## signal error recoverably
        echo ''
        return
    ## else, TODO: test ver_num for tri-numericity ~= N.N.N
    fi

    ## if [[ -n "${ver_num}" ]] ; then
    ## return it, with just a bit of "header"
    echo "MATE ${ver_num}"

} # end function get_MATE_version

### Get version for Xfce ... err, currently only for CURRENT_XFCE_MAJOR_VER :-(
function get_Xfce_version() {

    ## note local copies of readonly globals are forbidden in some bash versions :-(
    local -r FN_MESSAGE_PREFIX="${MESSAGE_PREFIX}::${FUNCNAME[0]}:"
    local -r FN_ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
    local ret_raw='' # Xfce devs return much more than what we want
    local ret_line=''
    local ver_str=''
#    local ret_val=''

    ret_raw="$(eval "${XFCE_VER_CL}" 2> /dev/null)"
    if [[ -z "${ret_raw}" ]] ; then
        ## signal error, but recoverably: let caller do the messaging
        echo ''
        return # value of 'last command executed within the function or script'
    fi

    ## if [[ -n "${ret_raw}" ]] ; then
    ret_line="$(echo "${ret_raw}" | head -n 1)"
#    echo "${MESSAGE_PREFIX} DEBUG: ret_line='${ret_line}'"
    if [[ -z "${ret_line}" ]] ; then
        ## signal error recoverably
        echo ''
        return
    fi

    ## if [[ -n "${ret_line}" ]] ; then
    ## parse it
    ver_str="${ret_line#* version }"
#    echo "${MESSAGE_PREFIX} DEBUG: ver_str='${ver_str}'"
    if [[ -z "${ver_str}" ]] ; then
        ## signal error recoverably
        echo ''
        return
    fi

    ## if [[ -n "${ver_str}" ]] ; then
    ## return it, with just a bit of replacement "header"
    echo "${CURRENT_XFCE_WM_NAME} ${ver_str}"

} # end function get_Xfce_version

### ------------------------------------------------------------------
### `source` fence
### ------------------------------------------------------------------

### I.e., don't run any following lines (to EOF) if this file is `source`d

if [[ "${BASH_SOURCE}" != "${0}" ]] ; then
    return 0
fi

### ----------------------------------------------------------------------
### payload
### ----------------------------------------------------------------------

### TODO: test for WMs in order of ... popularity? Is there a DistroWatch for WMs?

for wm_ver in \
    "$(get_Cinnamon_version)" \
    "$(get_MATE_version)" \
    "$(get_Xfce_version)" \
; do
    if [[ -n "${wm_ver}" ]] ; then
        echo "${wm_ver}"
        exit 0 # don't test further
    fi
done

### failed to detect any WMs
>&2 echo "${ERROR_PREFIX} failed to detect window manager"
exit 2 # reserve ret_val=1 for caller failure

### ------------------------------------------------------------------
### execution fence
### ------------------------------------------------------------------

## I.e., don't run any following lines (to EOF), period.

if [[ "${BASH_SOURCE}" == "${0}" ]] ; then
    # ... if this file is executed
    exit 3   # should never be reached
else
    # ... if this file is `source`d
    return 0 # should never be reached
fi

### ------------------------------------------------------------------
### testcases
### ------------------------------------------------------------------

## Currently only tested on:

## * my LMDE2 Cinnamon box
## * my LMDE2 MATE box
## * my LMDE2 Xfce box

## from both {GUI terminal} and {TUI terminal in tty1}. Regarding 'GUI terminal':

## * my LMDE2 Cinnamon box: ROXTerm in tty7
## * my LMDE2 MATE box:     ROXTerm in tty8 (not sure why not tty7)
## * my LMDE2 Xfce box:     Terminal in tty7

## -------------------------------------------------------------------
## examples
## -------------------------------------------------------------------

### Cut'n'paste any following stanza(s) into console after setting following constant:
THIS_FP='' # set to FQ path to this executable file

### Sample/expected output from my LMDE2 Cinnamon box (up-to-date when this script was run):
date ; "${THIS_FP}"
#> Tue Nov  7 15:05:44 MST 2017
#> Cinnamon 3.4.6

### Sample/expected output from my LMDE2 MATE box (up-to-date when this script was run):
date ; "${THIS_FP}"
#> Tue Nov  7 15:07:51 MST 2017
#> MATE 1.18.0

### Current sample/expected output from my LMDE2 Xfce box:
date ; "${THIS_FP}"
#> Tue Nov  7 15:09:00 MST 2017
#> xfwm4 4.10.1 (revision 3918e6b) for Xfce 4.10
