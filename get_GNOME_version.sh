#!/usr/bin/env bash

### Read GNOME 2 or 3 version as recommended @ https://unix.stackexchange.com/a/73225/38638
### Currently relies on `xmlstarlet` to run XPath queries:
### * for information, see http://xmlstar.sourceforge.net/
### * for Debian packaging, see https://packages.debian.org/search?keywords=xmlstarlet

### Copyright (C) 2017 Tom Roche <Tom_Roche@pobox.com>
### This work is licensed under the Creative Commons Attribution 4.0 International License.
### To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/

### ----------------------------------------------------------------------
### constants
### ----------------------------------------------------------------------

## messaging

THIS_FP="${0}"
THIS_DIR="$(readlink -f $(dirname ${THIS_FP}))" # FQ/absolute path
THIS_FN="$(basename ${THIS_FP})"
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

## GNOME file paths: see https://unix.stackexchange.com/a/73225/38638

G2_FP='/usr/share/gnome-about/gnome-version.xml' # GNOME2
G3_FP='/usr/share/gnome/gnome-version.xml'       # GNOME3
GFP=''                                           # what to use
GFP_ERROR="${ERROR_PREFIX} cannot find \`gnome-version.xml\` for GNOME2 or GNOME3"

## GNOME XPaths.
## Use terminology from Semantic Versioning rather than GNOME (sorry).

G3_MAJOR_XPATH='/gnome-version/platform'
G3_MINOR_XPATH='/gnome-version/minor'
G3_PATCH_XPATH='/gnome-version/micro'
G3_DATE_XPATH='/gnome-version/date'

### TODO: check that document paths are same in both majors!

G2_MAJOR_XPATH="${G3_MAJOR_XPATH}"
G2_MINOR_XPATH="${G3_MINOR_XPATH}"
G2_PATCH_XPATH="${G3_PATCH_XPATH}"
G2_DATE_XPATH="${G3_DATE_XPATH}"

GNOME_MAJOR_XPATH=''
GNOME_MINOR_XPATH=''
GNOME_PATCH_XPATH=''
GNOME_DATE_XPATH=''

## TODO: create a user-configurable output template

### ----------------------------------------------------------------------
### Payload
### ----------------------------------------------------------------------

## Setup

if   [[ -z "$(which xmlstarlet)" ]] ; then
    >&2 echo "${ERROR_PREFIX} ${THIS_FN} requires \`xmlstarlet\`, exiting ..."
    exit 2
elif [[ -r "${G2_FP}" ]] ; then
    GFP="${G2_FP}"
    GNOME_MAJOR_XPATH="${G2_MAJOR_XPATH}"
    GNOME_MINOR_XPATH="${G2_MINOR_XPATH}"
    GNOME_PATCH_XPATH="${G2_PATCH_XPATH}"
    GNOME_DATE_XPATH="${G2_DATE_XPATH}"
elif [[ -r "${G3_FP}" ]] ; then
    GFP="${G3_FP}"
    GNOME_MAJOR_XPATH="${G3_MAJOR_XPATH}"
    GNOME_MINOR_XPATH="${G3_MINOR_XPATH}"
    GNOME_PATCH_XPATH="${G3_PATCH_XPATH}"
    GNOME_DATE_XPATH="${G3_DATE_XPATH}"
else
    >&2 echo "${GFP_ERROR}, exiting ..."
    exit 3
fi

## Use `xmlstarlet` to parse gnome-version.xml

GNOME_MAJOR="$(xmlstarlet sel -t -v "${GNOME_MAJOR_XPATH}" ${GFP})"
if [[ ( "${?}" -ne 0 ) || ( -z "${GNOME_MAJOR}" ) ]] ; then
    >&2 echo "${ERROR_PREFIX} could not find GNOME major version, exiting ..."
    exit 4
fi

GNOME_MINOR="$(xmlstarlet sel -t -v "${GNOME_MINOR_XPATH}" ${GFP})"
if [[ ( "${?}" -ne 0 ) || ( -z "${GNOME_MINOR}" ) ]] ; then
    >&2 echo "${ERROR_PREFIX} could not find GNOME minor version, exiting ..."
    exit 5
fi

GNOME_PATCH="$(xmlstarlet sel -t -v "${GNOME_PATCH_XPATH}" ${GFP})"
if [[ ( "${?}" -ne 0 ) || ( -z "${GNOME_PATCH}" ) ]] ; then
    >&2 echo "${ERROR_PREFIX} could not find GNOME patch version, exiting ..."
    exit 6
fi

GNOME_DATE="$(xmlstarlet sel -t -v "${GNOME_DATE_XPATH}" ${GFP})"
if [[ ( "${?}" -ne 0 ) || ( -z "${GNOME_DATE}" ) ]] ; then
    >&2 echo "${ERROR_PREFIX} could not find GNOME date, exiting ..."
    exit 7
fi

echo -e "GNOME version=${GNOME_MAJOR}.${GNOME_MINOR}.${GNOME_PATCH} date=${GNOME_DATE}"
exit 0
