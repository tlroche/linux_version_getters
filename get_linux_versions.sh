#!/usr/bin/env bash

### Attempt to create a high-level commandline-query for basic information about one's host
### (at least, that part of the platform between the hardware and the specific application)
### with logging: takes log filepath on commandline, or uses own default filepath.

### USAGE: get_linux_versions.sh [path to logfile]

### TODO:
### * TESTCASES!
### * make compliant with Google Shell Style Guide currently @ https://google.github.io/styleguide/shell.xml
### * take commandline option `-a|--append` to append to given logfile path
### * define helper dir/folder, instead of requiring all helpers to be symlinked into *this* dir/folder.
### * move constants to properties file
### * if helpers not found, offer to install (and enable that offer!)
### * show $USAGE from `-h|--help`
### * see ./README.rst#TODOs

### Copyright (C) 2017-8 Tom Roche <Tom_Roche@pobox.com>
### This work is licensed under the Creative Commons Attribution 4.0 International License.
### To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
### Latest version (or pointer to same) should be available @
### https://bitbucket.org/tlroche/linux_version_getters/src/HEAD/get_linux_versions.sh

###----------------------------------------------------------------------
### constants
###----------------------------------------------------------------------

### for logging

declare -r THIS_FP="${BASH_SOURCE}"
declare -r LOGFILE_FP_ARG="${1}"
declare -r THIS_FN="$(basename ${THIS_FP})"
declare -r THIS_DIR="$(readlink -f $(dirname "${THIS_FP}"))" # FQ/absolute path

declare -r MESSAGE_PREFIX="${THIS_FN}:"
declare -r ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
declare -r DEBUG_PREFIX="${MESSAGE_PREFIX} DEBUG:"
declare -r DATE_FORMAT='%Y%m%d_%H%M'
declare -r TIMESTAMP="$(date +${DATE_FORMAT})"

## logfile_fp==path to logfile to write (pre-testing)

if [[ -n "${LOGFILE_FP_ARG}" ]] ; then
    logfile_fp="$(readlink -f "${LOGFILE_FP_ARG}")" # FQ/absolute path
else # -z "${LOGFILE_FP_ARG}"

    ## default path for logfile (if not given or writeable)
    declare -r DEFAULT_LOGFILE_FN="${THIS_FN%%\.sh}_log__$(hostname)__${TIMESTAMP}.txt"
    if [[ ( -z "${THIS_DIR}" ) || ( "${THIS_DIR}" == '/' ) ]] ; then
        DEFAULT_LOGFILE_DIR='/tmp'
    else
        DEFAULT_LOGFILE_DIR="${THIS_DIR}"
    fi
    declare -r DEFAULT_LOGFILE_FP="${DEFAULT_LOGFILE_DIR}/${DEFAULT_LOGFILE_FN}"
    # echo -e "${DEBUG_PREFIX} DEFAULT_LOGFILE_FP='${DEFAULT_LOGFILE_FP}'"
    logfile_fp="${DEFAULT_LOGFILE_FP}"

fi
# echo -e "${DEBUG_PREFIX} logfile_fp='${logfile_fp}'"

### "Marker file" paths for specific distros. TODO: complete the set.

declare -r DEBIAN_FILE_PATH='/etc/debian_version'

### helpers

#declare -r _CMD=''
#declare -r _CMD=""

## simple commands

declare -r CURRENT_TIME_CMD='date'
declare -r KERNEL_DETAILS_CMD='uname -rsv'
declare -r DISTRO_DETAILS_CMD='lsb_release -ds'
declare -r NONGRAPHICAL_DETAILS_CMD='gcc --version | head -n 1'
declare -r X_CMD="X -version 2>&1 | grep -ve '^$\|^[[:space:]]\|Current Operating System:'"
declare -r X_WM_CMD='x-window-manager --version | head -1'

## files

declare -r NVIDIA_VERSION_CMD="${THIS_DIR}/get_NVIDIA_version.sh"
declare -r GNOME_CMD="${THIS_DIR}/get_GNOME_version.sh"
declare -r WM_CMD="${THIS_DIR}/get_wm_version.sh"
declare -r HELPER_FILES_LIST="${NVIDIA_VERSION_CMD}
${GNOME_CMD}
${WM_CMD}"

###----------------------------------------------------------------------
### code
###----------------------------------------------------------------------

###----------------------------------------------------------------------
### functions
###----------------------------------------------------------------------

### TODO: move to standard-functions file
function setup_logging(){
    ## note local copies of readonly globals are forbidden in some bash versions :-(
    local -r FN_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${FUNCNAME[0]}:"
    local -r FN_ERROR_PREFIX="${FN_MESSAGE_PREFIX} ERROR:"
    local ret_val=''

    if   [[ -z "${logfile_fp}" ]] ; then
        >&2 echo "${FN_ERROR_PREFIX} logfile_fp not defined, exiting..."
        >&2 echo # newline
        exit 2
    elif [[ -r "${logfile_fp}" ]] ; then
        >&2 echo "${FN_ERROR_PREFIX} logfile='${logfile_fp}' exists: move or delete. Exiting..."
        echo # newline
        exit 3
    else
        touch "${logfile_fp}"
        ret_val="$?"
        if [[ ( "${ret_val}" -ne 0 ) || ( ! -w "${logfile_fp}" ) ]] ; then
            >&2 echo "${FN_ERROR_PREFIX} cannot write logfile='${logfile_fp}', exiting..."
            >&2 echo # newline
            exit 4
        else
            echo "${FN_MESSAGE_PREFIX} logging to file='${logfile_fp}'" | tee -a "${logfile_fp}"
            # No more in-function `tee` after this: do it in payload loop.
        fi
    fi
    return 0
} # end function setup_logging

function test_helpers() {
    ## note local copies of readonly globals are forbidden in some bash versions :-(
    local -r FN_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${FUNCNAME[0]}:"
    local -r FN_ERROR_PREFIX="${FN_MESSAGE_PREFIX} ERROR:"
    local ret_val=''

    ### test paths for commands without files? or just `which`?
#    test_commands
    test_helper_files

} # end function test_helpers

### Test that helper files exist
function test_helper_files() {
    ## note local copies of readonly globals are forbidden in some bash versions :-(
    local -r FN_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${FUNCNAME[0]}:"
    local -r FN_DEBUG_PREFIX="${FN_MESSAGE_PREFIX} DEBUG:"
    local -r FN_ERROR_PREFIX="${FN_MESSAGE_PREFIX} ERROR:"
    local fp=''

    while read fp ; do
#        echo "${FN_DEBUG_PREFIX} fp='${fp}'"
        if   [[ -z "${fp}" ]] ; then
            >&2 echo "${FN_ERROR_PREFIX} undefined helper file? exiting ..."
            exit 99
        elif [[ ! -x "${fp}" ]] ; then
            echo "${FN_ERROR_PREFIX} cannot execute helper file='${fp}', exiting ..."
            exit 99
        fi
    done <<< "${HELPER_FILES_LIST}"
    return 0
} # end function test_helper_files

### when are we?
function get_current_time(){
    ## note local copies of readonly globals are forbidden in some bash versions :-(
    local -r FN_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${FUNCNAME[0]}:"
    local -r FN_ERROR_PREFIX="${FN_MESSAGE_PREFIX} ERROR:"
    local ret_val=''
#    local -r CURRENT_TIME_STRING="$(eval "garbage")" # testing
    local -r CURRENT_TIME_STRING="$(eval "${CURRENT_TIME_CMD}")"

    if [[ ( "${ret_val}" -ne 0 ) || ( -z "${CURRENT_TIME_STRING}" ) ]] ; then
        >&2 echo "${FN_ERROR_PREFIX} error getting current time, exiting..."
        exit 5
    else
        echo "Information @ ${CURRENT_TIME_STRING}:"
    fi
    return 0
} # end function get_current_time

### Absolute basics: kernel, distro
function get_kernel_and_distro(){
    ## note local copies of readonly globals are forbidden in some bash versions :-(
    local -r FN_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${FUNCNAME[0]}:"
    local -r FN_ERROR_PREFIX="${FN_MESSAGE_PREFIX} ERROR:"
    local ret_val=''
    local cmd=''

    for cmd in \
        "${KERNEL_DETAILS_CMD}" \
        "${DISTRO_DETAILS_CMD}" \
    ; do
        echo -e "\$ ${cmd}"
        eval "${cmd}"
        if [[ "${ret_val}" -ne 0 ]] ; then
            >&2 echo "${FN_ERROR_PREFIX} error running command='${cmd}', exiting..."
            exit 5
        fi
    done
    return 0
} # end function get_kernel_and_distro

### Debian details via marker file. TODO: get for other distros.
function get_Debian_details(){
    ## note local copies of readonly globals are forbidden in some bash versions :-(
    local -r FN_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${FUNCNAME[0]}:"
    local -r FN_ERROR_PREFIX="${FN_MESSAGE_PREFIX} ERROR:"
    local ret_val=''
    local cmd=''

    if [[ -r "${DEBIAN_FILE_PATH}" ]] ; then
        for cmd in \
            "cat ${DEBIAN_FILE_PATH}" \
        ; do
            echo -e "\$ ${cmd}"
            eval "${cmd}"
            if [[ "${ret_val}" -ne 0 ]] ; then
                >&2 echo "${FN_ERROR_PREFIX} error running command='${cmd}', exiting..."
                >&2 echo # newline
                exit 6
            fi
        done
    fi
    return 0
} # end function get_Debian_details

### Other important non-graphical libraries, toolkits, etc.
### TODO: make these configurable!
function get_nongraphical_details(){
    ## note local copies of readonly globals are forbidden in some bash versions :-(
    local -r FN_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${FUNCNAME[0]}:"
    local -r FN_ERROR_PREFIX="${FN_MESSAGE_PREFIX} ERROR:"
    local ret_val=''
    local cmd=''

    for cmd in \
        "${NONGRAPHICAL_DETAILS_CMD}" \
    ; do
        echo -e "\$ ${cmd}"
        eval "${cmd}"
        if [[ "${ret_val}" -ne 0 ]] ; then
            >&2 echo "${FN_ERROR_PREFIX} error running command='${cmd}', exiting..."
            >&2 echo # newline
            exit 7
        fi
    done
    return 0
} # end function get_nongraphical_details

### Into the graphical world ...
### TODO: make these configurable!
function get_graphical_details(){
    ## note local copies of readonly globals are forbidden in some bash versions :-(
    local -r FN_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${FUNCNAME[0]}:"
    local -r FN_ERROR_PREFIX="${FN_MESSAGE_PREFIX} ERROR:"
    local ret_val=''
    local cmd=''

    for cmd in \
        "${NVIDIA_VERSION_CMD}" \
        "${X_CMD}" \
        "${X_WM_CMD}" \
        "${GNOME_CMD}" \
        "${WM_CMD}" \
    ; do
        echo -e "\$ ${cmd}"
        eval "${cmd}"
        if [[ "${ret_val}" -ne 0 ]] ; then
            >&2 echo "${FN_ERROR_PREFIX} error running command='${cmd}', exiting..."
            >&2 echo # newline
            exit 8
        fi
    done
    return 0
} # end function get_graphical_details

###----------------------------------------------------------------------
### main
###----------------------------------------------------------------------

setup_logging  # can now write log
echo | tee -a "${logfile_fp}" # newline

### payload loop
for cmd in \
    'test_helpers' \
    'get_current_time' \
    'get_kernel_and_distro' \
    'get_Debian_details' \
    'get_nongraphical_details' \
    'get_graphical_details' \
; do
    ## `eval "${cmd}"` may produce stderr/2, so gotta redirect to stdout/1 before piping
    eval "${cmd}" 2>&1 | tee -a "${logfile_fp}"
    ret_val="${PIPESTATUS[0]}"
    if [[ "${ret_val}" -ne 0 ]] ; then
        echo "${ERROR_PREFIX} running function='${cmd}', exiting..." 2>&1 | tee -a "${logfile_fp}"
        echo | tee -a "${logfile_fp}" # newline separator to stdout only
        exit 5
    fi
done

exit 0
